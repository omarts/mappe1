package Ntnu.Idatt2001.mapp1;

/**
 * Nurse is a subclass of the class Employee, and does not posess any special qualities.
 */
public class Nurse extends Employee{

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    @Override
    public String toString(){
        String a = "Fullname: " +getFullname()+"\nPersonNumber: "+getPersonnumber();
        return a;
    }
}
