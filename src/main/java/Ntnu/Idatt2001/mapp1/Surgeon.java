package Ntnu.Idatt2001.mapp1;

/**
 * The Surgeon class is a underclass of the Doctor class. It has the ability to diagnose patients, with the setDiagnosis methode,
 * this methode requires a Patient and a String.
 */
public class Surgeon extends Doctor{

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(Patient p, String Diagnosis){
        p.setDiagnose(Diagnosis);
    }
}
