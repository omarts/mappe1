package Ntnu.Idatt2001.mapp1;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The RemoveException class is a subclass of the class Exception. This class is used when
 * the remove methode is called, but the object given in the parameter is illigal as they are not found inside the Arraylists.
 */
class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String message){
        super(message);
    }
}

/**
 * The Department class is a object that contains two arrayLists, one ArrayList for Employees, and one for patients.
 * It also contains add methodes, get methodes, and for the objects inside the ArrayLists.
 * it also contains a remove methode which removes a given Person(Employee, or Patient) inside the arrays.
 *
 */
public class Department {
    String departmentName;
    ArrayList<Employee> Emploiees = new ArrayList<>();
    ArrayList<Patient> Patients = new ArrayList<>();

    /**
     * Constructor that needs name of the Department
     * @param DepartmentName
     */
    public Department(String DepartmentName){
        this.departmentName=DepartmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public void addEmployee (Employee Employee){
        Emploiees.add(Employee);
    }
    public void addPasient (Patient Patient){
        Patients.add(Patient);
    }

    public ArrayList<Employee> getEmployees() {

        return Emploiees;
    }

    public ArrayList<Patient> getPatients() {

        return Patients;
    }

    /**
     * remove methode that removes a Person from either of the arraysLists if they are found inside them
     * Will give an exception if the person is not found within the arrayLists
     * @param PersonBeingRemoved
     * @throws IllegalArgumentException
     */
    public void remove(Person PersonBeingRemoved) throws IllegalArgumentException{
    try {
            boolean removed = false;
            for (int i = 0; i < Emploiees.size(); i++) {
                if (Emploiees.get(i).getPersonnumber().equalsIgnoreCase(PersonBeingRemoved.getPersonnumber())
                        && Emploiees.get(i).getFullname().equalsIgnoreCase(PersonBeingRemoved.getFullname())) {
                    Emploiees.remove(i);
                    System.out.println("The employee has been removed");
                    removed = true;
                }
            }
            for (int j = 0; j < Patients.size(); j++) {
                if (Patients.get(j).getPersonnumber().equalsIgnoreCase(PersonBeingRemoved.getPersonnumber())
                        && Patients.get(j).getFullname().equalsIgnoreCase(PersonBeingRemoved.getFullname())) {
                    Patients.remove(j);
                    System.out.println("The Patient has been removed");
                    removed = true;
                }
            }
            if (!removed) {
                throw new RemoveException("The person was not found, and hence not removed");
            }
        } catch (RemoveException e){
        System.out.println(e.getMessage());
    }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(getDepartmentName(), that.getDepartmentName()) &&
                Objects.equals(Emploiees, that.Emploiees) &&
                Objects.equals(getPatients(), that.getPatients());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName(), Emploiees, getPatients());
    }





    @Override
    public String toString(){
        String DepartmentString ="Department name:" +departmentName +"\n";
        return DepartmentString;
    }


}
