package Ntnu.Idatt2001.mapp1;

/**
 * The HospitalClient class is a class using diffrent methodes within the classes, Department,Employee,Patient,Hospital, and more.
 */
public class HospitalClient {
    public static void main(String[] args) {
        Hospital StOlav = new Hospital("St,Olav");

        HospitalTestData.fillRegisterWithTestData(StOlav);
        System.out.println(StOlav);
        StOlav.addDepartment("Peanut Allergy Section");
        Patient PeanutHater = new Patient("Peanut", "HaterXx","123GME456");
        StOlav.getDepartments().get(2).addPasient(PeanutHater);
        System.out.println("The new Department");
        System.out.println(StOlav.getDepartments().get(2));
        System.out.println("And the patients");
        System.out.println(StOlav.getDepartments().get(2).getPatients());
        System.out.println("\n\nNow he shall be removed\n");
        StOlav.getDepartments().get(2).remove(PeanutHater);

        System.out.println("Reprinting the patients:\n"+StOlav.getDepartments().get(2).getPatients());
        System.out.println("And now i will try to remove him again\n");
        StOlav.getDepartments().get(2).remove(PeanutHater);

    }
}
