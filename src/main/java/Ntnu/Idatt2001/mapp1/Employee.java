package Ntnu.Idatt2001.mapp1;

/**
 * the Employee class is a subclass of Person.  It has the ability to diagnose patients, with the setDiagnosis methode,
 *  * this methode requires a Patient and a String.
 */
public class Employee extends Person {

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    @Override
    public String toString(){
        String a = "Fullname: " +getFullname()+"\nPersonNumber: "+getPersonnumber();
        return a;
    }
}
