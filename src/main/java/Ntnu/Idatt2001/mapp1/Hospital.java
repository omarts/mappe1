package Ntnu.Idatt2001.mapp1;

import java.util.ArrayList;

/**
 * The Hospital class is a class containing an ArrayLists of Departments. It contais  a getMethode, addMethode.
 * and a toString.
 * it also contains the Hospital name, and a getmethode for it.
 */
public class Hospital {
    String hospitalName;
    ArrayList<Department> Departments = new ArrayList<Department>();

    /**
     * Constructor needs name of hospital.
     * @param hospitalName
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
    }

    public String gethopitalName(){return hospitalName;}

    public void addDepartment(String NewDepartmentName){
        Department NewDepartment = new Department(NewDepartmentName);
        Departments.add(NewDepartment);
    }

    public ArrayList<Department> getDepartments() {
        return Departments;
    }



    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\n' +
                ", Departments=" + Departments +
                '}';
    }
}
