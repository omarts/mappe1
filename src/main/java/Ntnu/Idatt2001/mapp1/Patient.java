package Ntnu.Idatt2001.mapp1;

/**
 * Diagnosable is a interface. it contains the setDiagnose
 * methode, letting a class set a diagnosis on another object.
 */
interface Diagnosable{
    void setDiagnose(String Diagnose);
}

/**
 * The class Patient is a subclass of the abstract class Person.
 * This class is used to describe a patient. This class also has a
 * getDiagnosis methode.
 */
public class Patient extends Person implements Diagnosable {
String diagnosis = "None";

    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return diagnosis;
    }


    @Override
    public String toString() {
        String a = "Fullname: " + getFullname() + "\nPersonNumber: " + getPersonnumber()
                + "\nDiagnosis: "+getDiagnosis();
        return a;
    }

    @Override
    public void setDiagnose(String Diagnose) {
        this.diagnosis = Diagnose;

    }
}
