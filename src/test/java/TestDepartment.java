import Ntnu.Idatt2001.mapp1.Department;
import org.junit.Test;
import Ntnu.Idatt2001.mapp1.Employee;
import Ntnu.Idatt2001.mapp1.Patient;

/**
 * The TestDepartment class is a class that testes the remove methode of the department class.
 */
public class TestDepartment {
    @Test
    public void TestAddAndRemoveExistingPerson(){
        Department ChildSection = new Department("ChildSection");
        Employee Employee1 = new Employee("Erica","Spenzer","201088186");
        Patient Patient1 = new Patient("Alex","Turner","02030189214");

        ChildSection.addEmployee(Employee1);
        ChildSection.addPasient(Patient1);

        ChildSection.remove(Employee1);
        ChildSection.remove(Patient1);
    }
    @Test
    public void TestIfRemoveNonExsistingPerson(){
        Department ChildSection = new Department("ChildSection");
        Employee Employee2 = new Employee("Fez","Alkami","2010823123");
        ChildSection.remove(Employee2);//employee was never added to ChilSection.
    }



}
