package Ntnu.Idatt2001.mapp1;

/**
 * Doctor is a subclass of Employee. it does contain the methode setDiagnosis,
 * this methode requres a Patient, and a string. The methode sets the diagnosis of the selected Patient.
 */
public abstract class Doctor extends Employee {

    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public void setDiagnosis(Patient p, String Diagnosis){
        p.setDiagnose(Diagnosis);
    }
}
