package Ntnu.Idatt2001.mapp1;

/**
 * The Person is a abstract class. it contains 3 strings which are set in the constructor.
 * These being firstname,lastname, and socialSecurityNumber. There are set and get methodes for all of these variables.
 */
abstract class Person {
    String firstName;
    String lastName;
    String socialSecurityNumber;

    /**
     * Basic constructor with three strings.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public String getFullname(){
        return firstName + " " + lastName;
    }

    public String getPersonnumber() {
        String personnumber = "";
        for(int i = 0; i<= 5; i++){
            personnumber += socialSecurityNumber.charAt(i);
        }

        return personnumber;
    }
    @Override
    public String toString(){
        String a = "Fullname: " +getFullname()+"\nPersonNumber: "+getPersonnumber();
    return a;
    }
}
